﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Xamarin.Forms;
using Plugin.CurrentActivity;

[assembly: Xamarin.Forms.Dependency(typeof(MobileVisionDemo.Droid.NativeClass))]
namespace MobileVisionDemo.Droid
{
    class NativeClass : Interface.ICameraPreview
    {
        private static String TAG = "NativeClass";

        public void StartPreviewCamera()
        {
            //Hàm được gọi từ PCL
            Log.Debug(TAG, "Gọi từ PCL: StartPreviewCamera");

            Context context = CrossCurrentActivity.Current.Activity;

            //start CameraPreview Activity
            Intent cameraIntent = new Intent(context, type: typeof(CameraActivity));
            context?.StartActivity(cameraIntent);
        }
    }
}