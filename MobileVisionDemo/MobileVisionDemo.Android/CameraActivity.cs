﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Gms.Vision;
using Android.Gms.Vision.Barcodes;
using static Android.Gms.Vision.Detector;
using Android.Graphics;
using AndroidCamera = Android.Hardware.Camera;
using CameraParameters = Android.Hardware.Camera.Parameters;

namespace MobileVisionDemo.Droid
{

    [Activity(Label = "CameraActivity", Name = "MobileVisionDemo.Droid.CameraActivity")]
    public class CameraActivity : AppCompatActivity,ISurfaceHolderCallback,IProcessor
    {
        private static String TAG = "CameraActivity";
        const int RequestCameraPermissionID = 1001;

        //User control, view
        private SurfaceView cameraPreview;
        private TextView tvBarcode;
        private SeekBar seekBarZoom;

        //Barcode reader
        private BarcodeDetector barcodeDetector;
        private CameraSource cameraSource;

        //Camera device used by Camerasource
        private AndroidCamera mCamera;
        private volatile bool isFlashLighting = false;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            Log.Debug(TAG, "onCreate");
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.cameraLayout);

            cameraPreview = FindViewById<SurfaceView>(Resource.Id.camera_SurfaceView);
            tvBarcode = FindViewById<TextView>(Resource.Id.camera_tvBarcode);
            seekBarZoom = FindViewById<SeekBar>(Resource.Id.camera_seekBar);

            var btnBack = FindViewById<Button>(Resource.Id.camera_btnBack);
            var btnFlashLight = FindViewById<Button>(Resource.Id.camera_btnFlash);

            btnBack.Click += BtnBack_Click;
            btnFlashLight.Click += BtnFlashLight_Click;

            //Attaching a lamda to SeekBar.ProgressChanged event
            seekBarZoom.ProgressChanged += (s, e) =>
            {
                Log.Debug(TAG, "The value of SeekBar is " + e.Progress);
                SetCameraParameters(mCamera, e.Progress);               
            };

            //Khởi tạo Barcode Detector
            barcodeDetector = new BarcodeDetector
                .Builder(this)
                .SetBarcodeFormats(BarcodeFormat.Code39 | BarcodeFormat.Code128)
                .Build();

            //Khởi tạo CameraSource
            cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .SetFacing(CameraFacing.Back)
                .SetRequestedFps(35.0f)
                .SetAutoFocusEnabled(true)
                .Build();

            cameraPreview.Holder.AddCallback(this);
            barcodeDetector.SetProcessor(this);
        }

      
        public void ReceiveDetections(Detections detections)
        {
            //Xử lý dữ liệu barcode trả về khi detected
            SparseArray items = detections.DetectedItems;
            if (items.Size() != 0)
            {
                tvBarcode.Post(() =>
                {
                    //Vibrator vib = (Vibrator)GetSystemService(VibratorService);
                    //vib.Vibrate(500);
                    //vib.Vibrate(VibrationEffect.CreateOneShot(500, 50));

                    //barcode
                    tvBarcode.Text = ((Barcode)items.ValueAt(0)).RawValue;

                    //text recognize
                    //StringBuilder strBuilder = new StringBuilder();
                    //for (int i = 0; i < items.Size(); ++i)
                    //{
                    //    strBuilder.Append(((TextBlock)items.ValueAt(i)).Value);
                    //    strBuilder.Append("\n");
                    //}
                    //txtResult.Text = strBuilder.ToString();
                });
            }
        }

        public void Release()
        {
            
        }

        public void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
            
        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            if (ActivityCompat.CheckSelfPermission(ApplicationContext, Android.Manifest.Permission.Camera) != Android.Content.PM.Permission.Granted)
            {
                //Request permission
                ActivityCompat.RequestPermissions(this, new string[]
                {
                    Android.Manifest.Permission.Camera
                }, RequestCameraPermissionID);
                return;
            }

            try
            {
                //Start Camera preview
                cameraSource.Start(cameraPreview.Holder);

                //Lấy đối tượng camera
                mCamera = GetCamera(cameraSource);
                if (mCamera == null)
                {
                    Log.Debug(TAG, "mCamera device is null");
                }                
            }
            catch (Exception e)
            {
                Log.Debug(TAG, e.Message);
            }
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            //Stop camera
            cameraSource.Stop();
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void BtnFlashLight_Click(object sender, EventArgs e)
        {
            //Xóa nội dung barcode
            tvBarcode.Text = "";
            TurnOnOffFlashLight();
        }

        /// <summary>
        /// Hàm lấy đối tượng camera device đang sử dụng trong CameraSource
        /// </summary>
        /// <param name="cameraSource"></param>
        /// <returns> Android.Hardware.Camera cameraDevice</returns>
        private AndroidCamera GetCamera(CameraSource cameraSource)
        {
            var javaHero = cameraSource.JavaCast<Java.Lang.Object>();
            var fields = javaHero.Class.GetDeclaredFields();
            foreach (var field in fields)
            {
                Log.Debug(TAG, "field: " + field.Type.CanonicalName);

                if (field.Type.CanonicalName.Equals("android.hardware.Camera", System.StringComparison.OrdinalIgnoreCase))
                {
                    field.Accessible = true;
                    var camera = field.Get(javaHero);
                    var mCameraDevice = (AndroidCamera)camera;

                    Log.Debug(TAG, "Found camera device from CameraSource!");
                    return mCameraDevice;
                }
            }

            Log.Debug(TAG, "Not Found camera device from CameraSource!");
            return null;
        }

        #pragma warning disable CS0618 // Type or member is obsolete
        /// <summary>
        /// Hàm thiết lập thông số cho camera -> tăng chất lượng đọc barcode
        /// </summary>
        /// <param name="seekBarProg"></param>
        private void SetCameraParameters(Android.Hardware.Camera mCamera, int seekBarProg)
        #pragma warning restore CS0618 // Type or member is obsolete
        {
            if (mCamera == null) return;

            CameraParameters mParams = mCamera.GetParameters();
            int brightMin  = mParams.MinExposureCompensation;
            int brightMax = mParams.MinExposureCompensation;
            int zoomMax = mParams.MaxZoom;
            int curZoom = mParams.Zoom;

            Log.Debug(TAG, "Camera brightness Min: " + brightMin + ", Max: " + brightMax);
            Log.Debug(TAG, "Camera Zoom/ZoomMax: " + curZoom + "/" + zoomMax);

            int brightSet = brightMin / 2;
            mParams.ExposureCompensation = brightSet;

            int zoomSet = 5;
            zoomSet = seekBarProg * zoomMax / 100;
            mParams.Zoom = zoomSet;

            Log.Debug(TAG, "Setting Brightness to " + brightSet);
            Log.Debug(TAG, "Setting Zoom to " + zoomSet);
            try
            {
                //Set thông số cho Camera
                mCamera.SetParameters(mParams);
            }
            catch(Exception e)
            {
                Log.Debug(TAG, "Set camera params error: " + e.Message);
            }  
        }

        /// <summary>
        /// Hàm điều khiển On/Off toggle đèn Flash camera
        /// </summary>
        private void TurnOnOffFlashLight()
        {
            if (mCamera == null) return;

            Log.Debug(TAG, "TurnOnOffFlashLight");

            var camParams = mCamera.GetParameters();

            if (isFlashLighting)
            {
                //Turn off
                camParams.FlashMode = CameraParameters.FlashModeOff;                
            } else
            {
                //Turn on
                camParams.FlashMode = CameraParameters.FlashModeTorch;
            }
            isFlashLighting = !isFlashLighting;
            mCamera.SetParameters(camParams);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            switch (requestCode)
            {
                case RequestCameraPermissionID:
                    if (grantResults[0] == Permission.Granted)
                    {
                        if (ActivityCompat.CheckSelfPermission(ApplicationContext, Android.Manifest.Permission.Camera) != Android.Content.PM.Permission.Granted)
                        {
                            //Request permission
                            ActivityCompat.RequestPermissions(this, new string[]
                            {
                                Android.Manifest.Permission.Camera
                            }, RequestCameraPermissionID);
                            return;
                        }

                        try
                        {
                            cameraSource.Start(cameraPreview.Holder);
                            //Lấy đối tượng camera
                            mCamera = GetCamera(cameraSource);
                            if (mCamera == null)
                            {
                                Log.Debug(TAG, "mCamera device is null");
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Debug(TAG, e.Message);
                        }
                    }
                    break;
            }
        }
    }
}