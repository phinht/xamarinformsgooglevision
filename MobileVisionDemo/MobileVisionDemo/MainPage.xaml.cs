﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace MobileVisionDemo
{
    public partial class MainPage : ContentPage
    {
        private Pokemon pokemon;

        public MainPage()
        {
            InitializeComponent();
            pokemon = new Pokemon() { Name = "Pikachu", Type = "Miumiu" };

            //Navigation page


            //labelName.Text = pokemon.Name;
            //labelType.Text = pokemon.Type;

            //DataBinding in code
            //labelName.SetBinding(Label.TextProperty, new Binding()
            //{
            //    Source = pokemon,
            //    Path = "Name"
            //});

            ////Databinding context cho 1 phần tử
            //labelType.BindingContext = pokemon;

            ////Databinding context cho cả đối tượng (binding luôn cho cả Name và Type)
            //PokemonLayout.BindingContext = pokemon;
        }

        private void BtnCameraPreview_Clicked(object sender, EventArgs e)
        {
            //gọi tới hàm thực hiện preview camera cho từng Platform
            //DependencyService.Register<Interface.ICameraPreview>();
            DependencyService.Get<Interface.ICameraPreview>().StartPreviewCamera();
        }
    }
}
