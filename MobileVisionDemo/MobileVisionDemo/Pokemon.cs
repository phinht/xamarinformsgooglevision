﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileVisionDemo
{
    class Pokemon
    {
        public String Name { get; set; }
        public String Type { get; set; }
    }
}
