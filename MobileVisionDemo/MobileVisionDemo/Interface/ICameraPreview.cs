﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileVisionDemo.Interface
{
    public interface ICameraPreview
    {
        void StartPreviewCamera();
    }
}
